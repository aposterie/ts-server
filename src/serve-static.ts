/**
 * serve-static
 * Copyright(c) 2010 Sencha Inc.
 * Copyright(c) 2011 TJ Holowaychuk
 * Copyright(c) 2014-2016 Douglas Christopher Wilson
 * MIT Licensed
 * @see https://github.com/expressjs/serve-static/blob/master/index.js
 * Commit: 2512555 on 7 Feb 2018
 */

import { Request, Response, NextFunction } from "express"
import encodeUrl from "encodeurl"
import escapeHtml from "escape-html"
import parseUrl from "parseurl"
import { resolve } from "path"
import send, { SendStream } from "send"
import * as url from "url"

interface Options {
  fallthrough: boolean
  redirect: boolean
  setHeaders(): void
  filter(path: string): boolean
  root: string
  maxage: number
  maxAge: number
}

export default function serveStatic(root: string, options?: Partial<Options>) {
  // copy options object
  const opts: Options = Object.create(options || null);

  // fall-though
  const fallthrough = opts.fallthrough !== false;

  // default redirect
  const redirect = opts.redirect !== false;

  // headers listener
  const setHeaders = opts.setHeaders;

  // setup options for send
  opts.maxage = opts.maxage || opts.maxAge || 0;
  opts.root = resolve(root);

  // construct directory listener
  const onDirectory = redirect
    ? createRedirectDirectoryListener()
    : createNotFoundDirectoryListener();

  return function serveStatic(req: Request, res: Response, next: NextFunction) {
    if (req.method !== "GET" && req.method !== "HEAD") {
      if (fallthrough) {
        return next();
      }

      // method not allowed
      res.statusCode = 405;
      res.setHeader("Allow", "GET, HEAD");
      res.setHeader("Content-Length", "0");
      res.end();
      return;
    }

    let forwardError = !fallthrough;
    const originalUrl = parseUrl.original(req)!;
    let path = parseUrl(req)!.pathname!;

    // make sure redirect occurs at mount
    if (path === "/" && originalUrl.pathname!.substr(-1) !== "/") {
      path = "";
    }

    if (opts.filter && !opts.filter(path)) {
      return next();
    }

    // create send stream
    const stream = send(req, path, opts);

    // add directory handler
    stream.on("directory", onDirectory);

    // add headers listener
    if (setHeaders) {
      stream.on("headers", setHeaders);
    }

    // add file listener for fallthrough
    if (fallthrough) {
      stream.on("file", () => {
        // once file is determined, always forward error
        forwardError = true;
      });
    }

    // forward errors
    stream.on("error", (err) => {
      if (forwardError || !(err.statusCode < 500)) {
        next(err);
        return;
      }

      next();
    });

    // pipe
    stream.pipe(res);
  }
}

/**
 * Collapse all leading slashes into a single slash
 */
function collapseLeadingSlashes(str: string) {
  let i = 0;
  for (; i < str.length; i++) {
    if (str[i] !== "/") {
      break;
    }
  }

  return i > 1
    ? `/${str.substr(i)}`
    : str;
}

 /**
 * Create a minimal HTML document.
 */
function createHtmlDocument(title: string, body: string) {
  return "<!DOCTYPE html>" +
    '<html lang="en">' +
    "<head>" +
    '<meta charset="utf-8">' +
    `<title>${title}</title>` +
    "</head>" +
    `<body>${body}</body>` +
    "</html>";
}

/**
 * Create a directory listener that just 404s.
 */
function createNotFoundDirectoryListener() {
  return function notFound(this: SendStream) {
    this.error(404);
  };
}

/**
 * Create a directory listener that performs a redirect.
 */
function createRedirectDirectoryListener() {
  return function redirect(this: SendStream, res: Response) {
    if (this.hasTrailingSlash()) {
      this.error(404);
      return;
    }

    // get original URL
    const originalUrl = parseUrl.original((this as any).req)!;

    // append trailing slash
    originalUrl.path = undefined
    originalUrl.pathname = collapseLeadingSlashes(`${originalUrl.pathname}/`);

    // reformat the URL
    const loc = encodeUrl(url.format(originalUrl));
    const escaped = escapeHtml(loc);
    const doc = createHtmlDocument("Redirecting", /*html*/`Redirecting to <a href="${escaped}">${escaped}</a>`);

    // send redirect response
    res.statusCode = 301;
    res.set({
      "Content-Type": "text/html; charset=UTF-8",
      "Content-Length": Buffer.byteLength(doc),
      "Content-Security-Policy": "default-src 'self'",
      "X-Content-Type-Options": "nosniff",
      Location: loc,
    });
    res.end(doc);
  };
}