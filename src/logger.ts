// This module is not used.

import * as fs from "fs-extra"
import { resolve } from "path"
import morgan from "morgan"

const LOGS = resolve(__dirname, "../logs");

const _remoteAddr = morgan["remote-addr"];
morgan.token("remote-addr", (req) =>
  req.headers["x-real-ip"] ||
  req.headers["x-forwarded-for"] ||
  _remoteAddr(req)
);

export default function () {
  return morgan("combined", {
    stream: fs.createWriteStream(resolve(LOGS, "access.log"), { flags: "a" })
  });
}
