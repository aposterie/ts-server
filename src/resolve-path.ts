/**
 * resolve-path
 * Copyright(c) 2014 Jonathan Ong
 * Copyright(c) 2015-2018 Douglas Christopher Wilson
 * MIT Licensed
 * @see https://github.com/pillarjs/resolve-path
 * Commit: 9b8e6ba on 23 Sep 2018
 */

import createError from "http-errors"
import { join, normalize, resolve, sep, isAbsolute } from "path"

const UP_PATH_REGEXP = /(?:^|[\\/])\.\.(?:[\\/]|$)/

/**
 * Resolve relative path against a root path
 */
export function resolvePath(rootPath: string, relativePath?: string): string {
  let path = relativePath!
  let root = rootPath

  // root is optional, similar to root.resolve
  if (arguments.length === 1) {
    path = rootPath
    root = process.cwd()
  }

  // containing NULL bytes is malicious
  if (path.includes("\0")) {
    throw createError(400, "Invalid path. Path cannot contain a NULL byte.")
  }

  // path should never be absolute
  if (isAbsolute(path)) {
    throw createError(400, "Invalid path. Path cannot be absolute.")
  }

  // path outside root
  if (UP_PATH_REGEXP.test(normalize(`.${sep}${path}`))) {
    throw createError(403)
  }

  // join the relative path
  return normalize(join(resolve(root), path))
}
