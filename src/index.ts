import * as fs from "fs-extra"
import { URL } from "url"
import express from "express"
import RateLimit from "express-rate-limit"

import serveStatic from "./serve-static"
import { resolvePath } from "./resolve-path"
import { ClientError, ServerError } from "./http-status-code"

import * as tsNode from "ts-node"
tsNode.register({ transpileOnly: true });

interface Config {
  root: string
  port: number
  cors: boolean
}

export async function start(config: Config) {
  await fs.ensureDir(config.root);

  const app = express();
  app.use(new RateLimit({ windowMs: 1000, max: 100 }));
  app.use((req, _, next) => {
    req.url = req.url.normalize()
    next()
  })

  if (config.cors) {
    app.use((_, res, next) => {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      next();
    });
  }

  app.disable("x-powered-by");
  app.set("view engine", "ejs");

  app.all(["/\\..*"], (_req, res) => {
    res.sendStatus(ClientError.Forbidden).end();
  });

  app.get(/.+\.ejs$/, (req, res, next) => {
    const location = resolvePath(config.root, `.${req.path}`);
    if (fs.existsSync(location)) {
      res.render(location, {
        location: new URL(`${req.protocol}://${req.hostname}${req.url}`),
        require,
      });
    } else {
      next();
    }
  });

  app.get(/.+\.ts$/, async (req, res, next) => {
    const location = resolvePath(config.root, `.${req.path}`);
    if (fs.existsSync(location)) {
      try {
        const module = await import(location);
        await module.default.call(app, req, res);
      } catch (e) {
        res.status(ServerError.InternalServerError);
        if (process.env.NODE_ENV === "development") {
          res.write(e.toString());
        }
        res.end();
      }
    } else {
      next();
    }
  });

  app.use(serveStatic(config.root));

  app.get("*", (_req, res) => {
    res.status(ClientError.NotFound).end();
  });

  app.listen(config.port);

  return app;
}
