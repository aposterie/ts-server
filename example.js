const { resolve } = require("path");
const { start } = require("ts-server");

const appRoot = resolve(__dirname, "../html");

start({
  root: appRoot,
  port: 8080,
}).then(app => {
  console.log("Server started.");
}).catch(console.error);
